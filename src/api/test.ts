import * as caller from "grpc-caller";
import {graphReadPort, graphWritePort, readProtoLocation, writeProtoLocation} from "./contants";

const readClient = caller(graphReadPort, readProtoLocation, 'GraphService');
const readTestClient = async () => {
  const call1 = await readClient.sinkClient_1({});
  const call2 = await readClient.sinkClient_2({});

  call1.on('data', data => {
    console.log(`client 1`, data)
  })

  call2.on('data', data => {
    console.log(`client 2`, data)
  })
}

readTestClient();


const writeClient = caller(graphWritePort, writeProtoLocation, 'GraphService');
const writeTestClient = async () => {
  const addEdge = await writeClient.addEdge({});
  // const removeEdge = await writeClient.removeEdge({});
  // const addVertex = await writeClient.addVertex({});
  // const removeVertex = await writeClient.removeVertex({});


  Array.from([1, 2, 3], async (x) => {
    await writeClient.addEdge({
      vertex: Math.ceil(Math.random() * x),
      node: Math.ceil(Math.random() * (x + (Math.random() * x))),
    });
  })

  // addVertex.on('data', data => {
  //   console.log(`client 2`, data)
  // })
}

writeTestClient();
