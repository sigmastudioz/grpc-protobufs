export const graphReadPort = '127.0.0.1:50050';
export const graphWritePort = '127.0.0.1:50051';
export const readProtoLocation = __dirname + '/server/graph-read.proto';
export const writeProtoLocation = __dirname + '/server/graph-write.proto';