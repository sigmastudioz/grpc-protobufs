import * as events from 'events'
import GRPCWrite from './server/write'
import GRPCRead from './server/read'
import Graph from "./server/graph";

/** Event Bus **/
const graphStream = new events.EventEmitter();

/** Graph Singleton **/
let GraphData = new Graph();


/** BootUp GRPC as CQRS **/
/** GRPC Read Server **/
try {
  const readServer = new GRPCRead(graphStream, GraphData);
  readServer.start();
} catch (e) {
  console.error(e)
}

/** GRPC Write Server **/
try {
  const writeServer = new GRPCWrite(graphStream, GraphData);
  writeServer.start();
} catch (e) {
  console.error(e)
}