import * as grpc from "grpc";
import * as protoLoader from "@grpc/proto-loader";
import {graphWritePort, writeProtoLocation} from "../contants";
import Graph from "./graph";

let graphStream: any;
let GraphData: Graph

export default class GRPCWrite {
  private server: any;
  private graphProto:any;

  constructor(graphStreamEvent: any, GraphDataInfo: Graph) {
    graphStream = graphStreamEvent;
    GraphData = GraphDataInfo;
    this.server = new grpc.Server();
    this.graphProto = grpc.loadPackageDefinition(protoLoader.loadSync(writeProtoLocation));
  }

  addEdge(call, callback) {
    try {
      const key = call.request.vertex;
      const node = call.request.node;

      if(key && node) {
        GraphData.addEdge(key, node);
        console.log(JSON.stringify(GraphData));
        graphStream.emit('updateGraph', GraphData);
        callback(null, { status: true, message: `Add Edge, vertex ${key}, node ${node}`,data: GraphData });
      } else {
        callback(null, { status: false, message: `Could not Add Edge, need vertex and node`, data: GraphData });
      }
    } catch (e) {
      console.error(e);
    }
  };

  removeEdge(call, callback) {
    try {
      const key = call.request.vertex;
      const node = call.request.node;

      if(key && node) {
        GraphData.removeEdge(key, node);
        const GraphInfo: Graph = GraphData.getGraph();
        console.log(GraphInfo);
        graphStream.emit('updateGraph',GraphInfo);
        callback(null, { status: true, message: `Remove Edge, vertex ${key}, node ${node}`, data: GraphData });
      } else {
        callback(null, { status: false, message: `Could not Remove Edge, need vertex and node`, data: GraphData });
      }
    } catch (e) {
      console.error(e);
    }
  };

  addVertex(call, callback) {
    try {
      const key = call.request.vertex;

      if(key) {
        GraphData.addVertex(key);
        const GraphInfo: Graph = GraphData.getGraph();
        console.log(GraphInfo);
        graphStream.emit('updateGraph', GraphData);
        callback(null, {status: true, message: `Vertex added, vertex ${key}`, data: GraphData});
      } else {
        callback(null, { status: false, message: `Could not Add Vertex, need a vertex`, data: GraphData });
      }
    } catch (e) {
      console.error(e);
    }
  };

  removeVertex(call, callback) {
    try {
      const status = GraphData.removeVertex(call.request.vertex);
      let message = "Vertex Found";
      if(!status) {
        message = "Vertex not found";
      } else {
        graphStream.emit('updateGraph', GraphData);
      }
      callback(null, { status, data: GraphData, message });
    } catch (e) {
      console.error(e);
    }
  };

  start() {
    const addEdge = this.addEdge;
    const removeEdge = this.removeEdge;
    const addVertex = this.addVertex;
    const removeVertex = this.removeVertex;

    try {
      this.server.addService(this.graphProto.writegraph.GraphService.service, { addEdge, removeEdge, addVertex, removeVertex });
      this.server.bind(graphWritePort, grpc.ServerCredentials.createInsecure());
      this.server.start();
      console.log('grpc write server running on port:', graphWritePort);
    } catch (e) {
      console.error(e);
    }
  }
}