import * as grpc from "grpc";
import * as protoLoader from "@grpc/proto-loader";
import * as HashMap from 'hashmap';

import { graphReadPort, readProtoLocation } from "../contants";
import Graph from "./graph";


let graphStream = null
let GraphData: Graph

export default class GRPCRead {
  private server: any;
  private graphProto:any;

  constructor(graphStreamEvent: any, GraphDataInfo: Graph) {
    graphStream = graphStreamEvent;
    GraphData = GraphDataInfo;
    this.server = new grpc.Server();
    this.graphProto = grpc.loadPackageDefinition(protoLoader.loadSync(readProtoLocation));
  }

  sinkClient_1 (stream){
    try {
      stream.write(GraphData);
      graphStream.on('updateGraph', (GraphData: HashMap) => {
        stream.write(GraphData);
      });
    } catch (e) {
      console.error(e);
    }
  };

  sinkClient_2 (stream) {
    try {
      stream.write(GraphData);
      graphStream.on('updateGraph', (GraphData: HashMap) => {
        stream.write(GraphData);
      });
    } catch (e) {
      console.error(e);
    }
  };

  start() {
    const sinkClient_1 = this.sinkClient_1;
    const sinkClient_2 = this.sinkClient_2;

    try {
      this.server.addService(this.graphProto.readgraph.GraphService.service, { sinkClient_1, sinkClient_2 });
      this.server.bind(graphReadPort, grpc.ServerCredentials.createInsecure());
      this.server.start();
      console.log('grpc read server running on port:', graphReadPort);
    } catch (e) {
      console.error(e);
    }
  }
}