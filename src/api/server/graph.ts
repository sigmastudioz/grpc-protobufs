import * as HashMap from 'hashmap';
import Node from "./node";

const DIRECTED = 'directed';
const UNDIRECTED = 'undirected';

class Graph {
  private edgeDirection: string;
  private nodes: HashMap;

  constructor(edgeDirection = DIRECTED) {
    this.nodes = new HashMap();
    this.edgeDirection = edgeDirection;
  }

  addVertex(key:any) {
    if (this.nodes.has(key)) {
      return this.nodes.get(key);
    }
    const vertex = new Node(key);
    this.nodes.set(key, vertex);
    return vertex;
  }

  removeVertex(key:any) {
    const current = this.nodes.get(key);
    if (current) {
      Array.from(this.nodes.values()).forEach((node: Node) => node.removeAdjacent(current));
    }
    return this.nodes.delete(key);
  }

  addEdge(source:any, destination:any) {
    const sourceNode = this.addVertex(source);
    const destinationNode = this.addVertex(destination);

    sourceNode.addAdjacent(destinationNode);

    if (this.edgeDirection === UNDIRECTED) {
      destinationNode.addAdjacent(sourceNode);
    }

    return [sourceNode, destinationNode];
  }

  removeEdge(source:any, destination:any) {
    const sourceNode = this.nodes.get(source);
    const destinationNode = this.nodes.get(destination);

    if (sourceNode && destinationNode) {
      sourceNode.removeAdjacent(destinationNode);

      if (this.edgeDirection === UNDIRECTED) {
        destinationNode.removeAdjacent(sourceNode);
      }
    }

    return [sourceNode, destinationNode];
  }

  getGraph() {
    return this.nodes
  }
}

export default Graph;
