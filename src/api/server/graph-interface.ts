export interface Graph {
  message: string
}

export interface Node {
  vertrex: string
  edge: string
  message: string
}

export interface Map {
  key: string
  data: GraphList
}

export interface GraphList extends Array<Graph>{}