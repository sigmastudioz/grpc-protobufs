import * as HashSet from  'hashset';

export default class Node {
  private value: any;
  private adjacents: HashSet;

  constructor(value) {
    this.value = value;
    this.adjacents = new HashSet();
  }

  addAdjacent(node) {
    this.adjacents.add(node);
  }

  removeAdjacent(node) {
    return this.adjacents.remove(node);
  }

  isAdjacent(node) {
    return this.adjacents.has(node);
  }

  getAdjacents() {
    return Array.from(this.adjacents);
  }
}