# GPRC ProtoBuf

> In this exercise, we would like to understand your ability to work with network 
  interfaces and data structures, as well as best practices and code structure in a typescript program.
   
  Problem statement:
  Networked systems and the graph data structure are two fundamental building
  blocks of our current interconnected software landscape. Using Typescript tools and
  libraries of your choice, create an implementation of a client and server
  program that can maintain the state of a graph and inform clients of the
  updates that are made to the nodes of a generic graph data structure.   

Server:
- Should act as the source of truth for the graph store.
- The server starts with an empty graph (no persistence necessary).
- Server will update all clients of any changes that are made to the graph.
   
Client:
- Client will connect and be able to add/remove vertices/edges.
- Any client can connect to the server at any time.
- On connecting the client should get a full state of the graph followed by changes as they happen.
- You will need to add a test client that can load the graph with some interesting datasets.
   
Test Setup:
- The server should have at least 1 source client and 2 sink clients.
- Add unit test to verify the functionality.
   
>  For communication, please use gRPC/protobufs. Make sure to use typed interfaces.
  You may use other third-party packages as appropriate; for instance, you may 
  implement your own graph data structure or use a library.
   
  We are interested in the structure and readability of the code; use whatever
  standard libraries and coding best practices and methodologies you feel are
  suited for this task. Once complete, send the files back to me along with a
  summary explanation on how to run your code, the expected output, as well as
  any insights or observations you had along the way.
   
  The problem scoped to be a day of work, but if you get
  stuck or have clarifying questions, feel free to reach out to the team by
  replying to this email.  Let us know how long you spent overall on the coding
  portion once completed.

## Running Code

``` bash
# Install dependencies
npm install
or
yarn install

# Start GRPC CRQS INSTANCE
npm run start
or
yarn start

# Run Test
npm run test
or
yarn test

Output is printed in server console
```


